def quick_sort(left,right,nums):
    if  left< right:
        base = partition(left,right,nums)
        quick_sort(left,base-1,nums)
        quick_sort(base+1,right,nums)
    return nums

def partition(left,right,nums):
    pivot = nums[left]
    while left < right:
        while left< right and nums[right]>=pivot:
            right-=1
        nums[left] = nums[right]
        while left<right and nums[left]<=pivot:
            left+=1
        nums[right] = nums[left]
    nums[left]=pivot
    return left

def bubble_sort(nums):
    for i in range(len(nums)-1):
        for j in range(i+1,len(nums)):
            if nums[i]>nums[j]:
                nums[i],nums[j] = nums[j], nums[i]
    return nums

def heap_sort(nums):
    build_heap(nums,0,len(nums)-1)
    for i in range(len(nums)-1,-1,-1):
        if nums[0]>nums[i]:
            # nums[0], nums[i] = nums[i], nums[0]
            heapfy(0,i,nums)
    return nums

def build_heap(nums,i,length):
    for i in range((length-1)//2,-1,-1):
        heapfy(nums,i,length)

def heapfy(nums,i,length):
    left = 2*i+1
    right = 2*i+2
    if left < length and nums[left]>nums[i]:
        large = left
    else:
        large = i
    if left < length and nums[right]>nums[large]:
        large = right
    if nums[large]!=nums[i]:
        heapfy(nums,large,length)


if __name__ == '__main__':
    nums = [5,2,8,4,1,2,6,9]
    res = quick_sort(0,len(nums)-1,nums)
    print(res)

    res = bubble_sort(nums)
    print(res)

    res = heap_sort(nums)
    print(res)